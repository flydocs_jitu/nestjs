import { Injectable } from '@nestjs/common';
// import { FormDTO } from './form.dto';
import { FormFieldEntity } from './form.field.enity';

@Injectable()
export class FormFieldRepository {
    async create(createFormDTO) {
        return await FormFieldEntity.bulkCreate(createFormDTO);
    }

}