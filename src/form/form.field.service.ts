import { Injectable } from '@nestjs/common';
import { FormFieldEntity } from './form.field.enity';
import { FormFieldRepository } from './form.field.repository';

import { FormDTO } from './form.dto';

@Injectable()
export class FormFieldService {
    constructor(
        private formFieldRepository: FormFieldRepository,
    ) {}

    async createFormFields(createformDTO) {
        const form = await this.formFieldRepository.create(createformDTO);
        return { form };
    }


   
}