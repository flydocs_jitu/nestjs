import { Module } from '@nestjs/common';
import { FormController } from './form.controller';
import { FormService } from './form.service';
import { FormFieldService } from './form.field.service';
import { FormRepository } from './form.repository';
import { FormFieldRepository } from './form.field.repository';
import { FormDataService } from './form.data.service';
import { FormDataRepository } from './form.data.repository';


@Module({
  controllers: [FormController],
  exports: [FormRepository],
  providers: [FormRepository, FormService,FormFieldRepository, FormFieldService, FormDataService, FormDataRepository],
})
export class FormModule {}
