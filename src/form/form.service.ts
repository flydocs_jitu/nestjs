
import { Injectable } from '@nestjs/common';
import { FormRepository } from './form.repository';
import { FormDTO } from './form.dto';

@Injectable()
export class FormService {
    constructor(
        private formRepository: FormRepository,
    ) {}

    async create(createformDTO) {
        const form = await this.formRepository.create(createformDTO);
        return form;
    }

    async findByTitle(title: string) {
       return await this.formRepository.findAll(title);
    }
}
