import { Model,Column,DataType,Table,ForeignKey } from 'sequelize-typescript';

  import { FormEntity } from './form.entity';
  import { FormFieldEntity } from './form.field.enity';


@Table({ tableName: 'fd_form_data' })
export class FormDataEntity extends Model {
    @Column({
      primaryKey: true,
      type: DataType.INTEGER,
      autoIncrement: true,
      allowNull: false,
    })
    id: number;

    @Column({
      allowNull: false,
      type: DataType.STRING,
    })
    @ForeignKey(() => FormEntity)
    form_id: string;

    @Column({
      allowNull: false,
      type: DataType.INTEGER,
      defaultValue: DataType.INTEGER,
    })
    @ForeignKey(() => FormFieldEntity)
    field_id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    value: string;

}