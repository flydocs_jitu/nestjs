import { Controller,Get,Post,Body,HttpCode,HttpStatus,Query } from '@nestjs/common';
import { FormDTO, GetFormPathParams } from './form.dto';
import { FormService } from './form.service';
import { FormFieldService } from './form.field.service';
import { FormDataService } from './form.data.service';


// import { Response as ResponseCustom } from '@nestjs/common';

@Controller('form')
export class FormController {
    constructor(
        private formService: FormService,
        private formFieldService: FormFieldService,
        private formDataService: FormDataService
    ) {}
   
    @Get('/get_form')
    @HttpCode(HttpStatus.OK)
    async getFormtByTitle(@Query('form_title') form_title: string) {
        return await this.formService.findByTitle(form_title);
    }

    @Post('add_form')
    @HttpCode(HttpStatus.CREATED)
    async createPost(@Body() FormDTO: FormDTO) {
        let form = await this.formService.create(FormDTO);
        const { title, ...formField } = FormDTO;
        if(form){
            const fieldEntries = Object.entries(formField);
            let fieldAllEntities = [];
            fieldAllEntities = fieldEntries.map(([field_name, field_type]) => ({
                field_name,
                field_type,
                form_id: form.id
            }));
           this.formFieldService.createFormFields(fieldAllEntities);
        }
        return form;
    }

    @Post('add_form_data')
    @HttpCode(HttpStatus.CREATED)
    async postFormData(@Body() FormDTO: FormDTO) {
        return await this.formDataService.create(FormDTO);
    }
}