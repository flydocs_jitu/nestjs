import { IsBoolean, IsNotEmpty, IsNumber, IsString, IsUUID } from 'class-validator';

export class FormDTO {
    @IsNotEmpty()
    @IsUUID(4)
    id: string;

    @IsNotEmpty()
    @IsString()
    title: string;
}

// export class CreateFormDTO extends OmitType(FormDTO, ['id'] as const) {}

export class GetFormPathParams {
    @IsNotEmpty()
    @IsUUID(4)
    id: string;
}
