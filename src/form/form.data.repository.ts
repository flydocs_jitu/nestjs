import { Injectable } from '@nestjs/common';
// import { FormDTO } from './form.dto';
import { FormDataEntity } from './form.data.entity';

@Injectable()
export class FormDataRepository {
    async create(createDataDTO) {
        return await FormDataEntity.bulkCreate(createDataDTO);
    }

}
