import { Model,Column,DataType,Table,ForeignKey } from 'sequelize-typescript';
// import { FormFieldDTO } from './field.dto';
import { FormEntity } from './form.entity';

  @Table({ tableName: 'fd_form_fields' })
  export class FormFieldEntity extends Model {
    @Column({
      primaryKey: true,
      type: DataType.INTEGER,
      autoIncrement: true,
      allowNull: false,
    })
    id: number;
  
    @Column({
      allowNull: false,
      type: DataType.STRING,
    })
    @ForeignKey(() => FormEntity)
    form_id: string;
  
    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    field_name: string;
  
    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    field_type: string;
  }