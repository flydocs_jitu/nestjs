import { Model, Column, DataType, Table, HasMany } from 'sequelize-typescript';
import { FormDTO } from './form.dto';
import { FormFieldEntity } from './form.field.enity';


@Table({ tableName: 'fd_form_master' })
export class FormEntity extends Model<FormDTO> {

    @Column({
        primaryKey: true,
        type: DataType.UUID,
        defaultValue: DataType.UUIDV4,
    })
    id: string;

    @Column({
        type: DataType.STRING,
        allowNull: false,
    })
    title: string;

    @HasMany(() => FormFieldEntity)
    fields: FormFieldEntity[];

}