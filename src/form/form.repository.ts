import { Injectable } from '@nestjs/common';
import { FormEntity } from './form.entity';
import { FormFieldEntity

 } from './form.field.enity';
@Injectable()
export class FormRepository {
    async create(createFormDTO) {
        return await FormEntity.create(createFormDTO);
    }

    async findAll(title : string) {
        return await FormEntity.findAll({
            where: { title },
            include: [
                {
                    model: FormFieldEntity,
                    as: 'fields'
                }
            ]
        });
    }
}
