import { Injectable } from '@nestjs/common';
import { FormDataRepository } from './form.data.repository';


@Injectable()
export class FormDataService {
    constructor(
        private formDataRepository: FormDataRepository,
    ) {}

    async create(createDataDTO: any) {
        const { form_id, ...dataField } = createDataDTO;
        const dataEntries = Object.entries(dataField);
        let dataAllEntities = [];
        dataAllEntities = dataEntries.map(([field_id, value]) => ({
            field_id,
            value,
            form_id:form_id
        }));
        const form = await this.formDataRepository.create(dataAllEntities);
        return form;
    }
}