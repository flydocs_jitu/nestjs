import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FormModule } from './form/form.module';
import { SequelizeModule } from '@nestjs/sequelize';
import { FormEntity } from './form/form.entity';
import { FormFieldEntity } from './form/form.field.enity';
import { FormDataEntity } from './form/form.data.entity';

@Module({
  imports: [
    FormModule,
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: '10.102.32.196',
      port: 3306,
      username: 'development',
      password: 'Admin@123',
      database: 'nestdb',
      autoLoadModels: true,
      synchronize: true,
    }),
    SequelizeModule.forFeature([FormEntity,FormFieldEntity, FormDataEntity]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
