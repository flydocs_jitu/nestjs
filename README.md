http://127.0.0.1:3000/form/add_form# NestJs



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/flydocs_jitu/nestjs.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/flydocs_jitu/nestjs/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## POST API : Save Dynamic Form
- This API creates a dynamic form with customized fields. Where all fields will 
```
Route: POST > http://127.0.0.1:3000/form/add_form
Request JSON Body:
{
    "title":"form-A",
    "f_name": "string",
    "l_name": "String",
    "phone": "string"
}
- title: This is the Form name, where data type is string
- f_name: This is for field name where data type is string
- l_name: This is for field name where data type is string
- phone: This is for field name where data type is string
```


[Table Structure](#)

```
CREATE TABLE `fd_form_master` (
	`id` CHAR(36) NOT NULL COLLATE 'utf8mb4_bin',
	`title` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`createdAt` DATETIME NOT NULL,
	`updatedAt` DATETIME NOT NULL,
	PRIMARY KEY (`id`) USING BTREE
)

CREATE TABLE `fd_form_fields` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`form_id` VARCHAR(255) NOT NULL DEFAULT '' COLLATE 'utf8mb4_unicode_ci',
	`field_name` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`field_type` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`createdAt` DATETIME NOT NULL,
	`updatedAt` DATETIME NOT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
```

## GET API : Get Form & its fields 
- return form with Form data and it contains all its dynamic fields & related details.
- where [form_title]() is a GET variable, passes the value as Form Title 
```
Route: GET > http://127.0.0.1:3000/form/get_form?form_title=form-A
API Response:
[
    {
        "id": "d9bc3f48-173d-4877-88fd-390c1e5b6d64",
        "title": "form-A",
        "fields": [
            {
                "id": 7,
                "form_id": "d9bc3f48-173d-4877-88fd-390c1e5b6d64",
                "field_name": "f_name",
                "field_type": "string",
            },
            {
                "id": 8,
                "form_id": "d9bc3f48-173d-4877-88fd-390c1e5b6d64",
                "field_name": "l_name",
                "field_type": "String",
            },
            {
                "id": 9,
                "form_id": "d9bc3f48-173d-4877-88fd-390c1e5b6d64",
                "field_name": "phone",
                "field_type": "string",
            }
        ]
    }
]
```


## POST API : Add Data in Dynamic Form by Form & Field Ids 
- This API will add data for dynamic form.
- As the form is dynamic so here we have implemented a key-value pair structure, where here data will be stored as field id and field value



```
Route: POST > http://127.0.0.1:3000/form/add_form_data
Request JSON Body:
{
    "form_id": "d9bc3f48-173d-4877-88fd-390c1e5b6d64",
    "1":"Jitu",
    "2":"Parmar",
    "3":"987654321"
}
Here, form_id is the main Form Id, based on this data will be stored. and remaining data is a pairing of Field_Id and Field_value as below mentioned table.
```

[Table Structure](#)
```
CREATE TABLE `fd_form_data` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`form_id` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`field_id` INT(11) NOT NULL,
	`value` VARCHAR(255) NOT NULL COLLATE 'utf8mb4_unicode_ci',
	`createdAt` DATETIME NOT NULL,
	`updatedAt` DATETIME NOT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
 - form_id is a foreign key that references to fd_form_master
 - field_id is a foreign key that references to fd_form_fields.
```